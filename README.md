# MushROM
## Overview
MushROM is a mod for Super Mario 63 that implements a range of utilities to facilitate experimentation and research. It is a spiritual successor to the [SM63 Practice Hack](https://github.com/KevinKib/SM63PracticeHack). It can be loaded by [Flapman](https://gitlab.com/GTcreyon/flapman).

## Build
### Requirements
- [Python](https://www.python.org/)
- The contents of this repository

### Method
To build the mod, run the `build.py` script with Python.

```python ./build.py```

This will build the mod to the `build` directory.

## Licensing and Attribution
- This project is licensed under AGPL-3.0-or-later. See the LICENSE file included for legal information.
- Super Mario 63 was created by [Runouw](http://runouw.com/index.php), and is a transformative work based on intellectual properties belonging to Nintendo.
- Original Practice Hack written by [Sekanor](https://github.com/KevinKib). Code and ideas were carried over in places.
