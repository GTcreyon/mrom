#!/usr/bin/env python
# Collects MROM source files into a BlueShell patch, and copies patch files to the build folder.

from pathlib import Path
from shutil import copytree
from zipfile import ZipFile

MODULE_FOLDER_NAME = "modules"
CYCLE_FOLDER_NAME = "cycle"
CMD_FOLDER_NAME = "commands"
TERMLINES_PATH = Path("termlines.as")
CORE_PATH = Path("core/")
EXTRAS_PATH = Path("extra/")
COREPATCH_PATH = Path("src/core.patch")
MACROS = {
    "SA": "var wb = wBool(args[",
    "SB": "])\nvar val = ",
    "SC": "\nvar wt = wToggle(wb, val)",
    "WG": "wb, val, wt",
}

def compile_modules(folder: Path, encapsulate: bool = False) -> str:
    """Compile modules from a directory into a single script."""
    output = ""
    for file in folder.iterdir():
        if file.suffix != ".as":
            continue

        with file.open("r") as f:
            text = f.read()

        if encapsulate:
            output += f"""{file.stem} = function(){{
                {text}
            }}
            {file.stem}()
            """
        else:
            output += f"{text}\n"
    return output


def compile_commands(folder: Path, encapsulate: bool = False) -> (str, list):
    """Collate commands from the command folder into a switch statement."""
    output = ""
    help_entries = []
    for file in folder.iterdir():
        if file.suffix != ".as":
            continue

        with file.open("r") as f:
            text = f.read()

        if not text.startswith("//"):
            continue

        header = text[2 : text.index("\n")].split("|")  # get command header
        aliases = header[0].split(",")
        arguments = header[1].split(",")
        if arguments[0] == "":
            arguments.clear()
        description = header[2]
        help_entries.append([aliases, arguments, description])

        top = ""
        for alias in aliases:
            top += 'case "' + alias + '":\n'  # form command aliases
        if encapsulate:
            output += f"""{top}cmd{aliases[0]} = function()
            \u007b
            {text}
            \u007d
            cmd{aliases[0]}()
            break\n"""
        else:
            output += f"""{top}{text}
            break\n"""
    for (macro, val) in MACROS.items():
        output = output.replace(f"@{macro}", val)
    return output, help_entries


def compile_aliases(file: Path, numerical: bool) -> str:
    """Collate aliases from a file into a switch statement."""
    output = ""
    with file.open("r") as f:
        line = f.readline()
        while line != "":
            if line[0] == "@":
                # Remove @ from the startand newline from the end.
                val = line[1:-1]
                if not numerical:
                    val = f'"{val}"'
                output += f"return {val}\n"
            else:
                # Remove newline from the end.
                output += f'case "{line[:-1]}":\n'

            line = f.readline()
    return output


def generate_docs(data: list) -> str:
    """Generate documentation using the headers of commands."""
    output = "# Commands\n"
    for command in data:
        aliases = command[0]
        arguments = command[1]
        description = command[2]
        output += f"## {aliases[0]}\n{description}\n\n"
        if len(aliases) > 1:
            alias_string = ", ".join(aliases[1:])
            output += f"**Aliases:** {alias_string}\n\n"
        if len(arguments) > 0:
            argument_string = ", ".join(arguments)
            output += f"**Arguments:** {argument_string}\n\n"
    return output


def build_src() -> None:
    """Build the source files and compress to a ZIP archive."""
    input_dir = Path()
    build_dir = Path("build")
    doc_path = Path("build/help.md")
    out_path = Path("output/mrom.zip")

    for root, dirs, files in build_dir.walk(top_down=False):
        for name in files:
            (root / name).unlink()
        for name in dirs:
            (root / name).rmdir()

    core_dir = input_dir / CORE_PATH
    module_folder = core_dir / MODULE_FOLDER_NAME
    cycle_folder = core_dir / CYCLE_FOLDER_NAME
    cmd_folder = core_dir / CMD_FOLDER_NAME

    print("Copying extras...")
    copytree(input_dir / EXTRAS_PATH, build_dir, dirs_exist_ok=True)
    print("Extras copied.")

    print("Loading base patch...")
    with Path(core_dir, "base.patch").open("r") as f:
        compiled_script = f.read()
    print("Base patch loaded.")

    print("Compiling modules...")
    # Load, combine and insert modules
    compiled_modules = compile_modules(module_folder)
    compiled_script = compiled_script.replace("//$MODULE", compiled_modules)
    print("Modules inserted.")

    print("Compiling keynames...")
    # Load, combine and insert keynames
    compiled_keynames = compile_aliases(module_folder / "keynames.txt", numerical=True)
    compiled_script = compiled_script.replace("//$KEYNAME", compiled_keynames)
    print("Keynames inserted.")

    print("Compiling warp aliases...")
    # Load, combine and insert warp aliases
    compiled_warpnames = compile_aliases(module_folder / "warpnames.txt", numerical=False)
    compiled_script = compiled_script.replace("//$WARPNAME", compiled_warpnames)
    print("Warp aliases inserted.")

    print("Compiling watch aliases...")
    # Load, combine and insert watch aliases
    compiled_watchnames = compile_aliases(module_folder / "watchnames.txt", numerical=False)
    compiled_script = compiled_script.replace("//$WATCHNAME", compiled_watchnames)
    print("Warp aliases inserted.")

    print("Compiling cycle...")
    # Load, combine and insert the cycle
    compiled_cycle = compile_modules(cycle_folder, encapsulate=True)
    compiled_script = compiled_script.replace("//$CYCLE", compiled_cycle)
    print("Cycle inserted.")

    print("Compiling commands...")
    # Load, combine, format and insert commands
    compiled_commands, help_data = compile_commands(cmd_folder)
    compiled_script = compiled_script.replace("//$CMD", compiled_commands)
    print("Commands inserted.")

    print("Adding termlines...")
    termlines = ""
    with (core_dir / TERMLINES_PATH).open("r") as f:
        termlines = f.read()
    compiled_script = compiled_script.replace("//$TERMLINE", termlines)
    print("Added termlines.")

    print("Generating documentation...")
    doc_path.parent.mkdir(parents=True, exist_ok=True)
    with doc_path.open("w") as f:
        f.write(generate_docs(help_data))
    print(f"Documentation generated! Saved to {doc_path}")

    print("Writing script...")
    build_dir.parent.mkdir(parents=True, exist_ok=True)
    outpath = build_dir / "src/core.patch"
    with outpath.open("w") as f:
        f.write(compiled_script)
    print(f"Core patch complete! Saved to {outpath}")

    print("Zipping mod...")
    out_path.parent.mkdir(parents=True, exist_ok=True)
    with ZipFile(out_path, "w") as f:
        for root, dirs, files in build_dir.walk():
            for file in files:
                if file == "mrom.zip":
                    continue
                path = (root / file)
                print(f"Zipped file {path}")
                f.write(path, path.relative_to(build_dir))
    print("All done!")


if __name__ == "__main__":
    build_src()
