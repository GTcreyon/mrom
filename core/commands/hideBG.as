//hidebg,hbg|wTOGGLE|Toggle hiding of the background layer.
@SA 1 @SB !_root.FarBackground._visible @SC
_root.Background._visible = !wt
_root.CloseBackground._visible = !wt
_root.MidBackground._visible = !wt
_root.FarBackground._visible = !wt
togResp("Hide BG", @WG)