//setshine,shine|NUMBER,wTOGGLE|Sets the state of the given shine ID.
var id = args[1]
@SA 2 @SB _root.Star[id] @SC
_root.Star[id] = wt
_root.CalculateStars()
togResp("Shine " + id, @WG)
