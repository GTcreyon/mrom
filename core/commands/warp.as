//warp,w|STRING:room_code,*NUMBER:x_pos,*NUMBER:y_pos,*BOOL:reset_music,*STRING:transition_type|Warps the player to the specified room.
var roomCode = args[1]
var target = warpAlias(roomCode)
_root.blueshell.warpRoom = target

if(args[3] === undefined || args[3] === "*") {
	_root.blueshell.warpLocation = warpLocation(target)
} else {
	_root.blueshell.warpLocation = [args[2], args[3]]
}

_root.blueshell.warpResetMusic = wBool(args[4])
if(args[5] === undefined) {
	_root.blueshell.warpTransition = "StarIn"
} else {
	_root.blueshell.warpTransition = args[5]
}

var warpMsg = roomCode
if (target !== roomCode) {
	warpMsg += " [" + target + "]"
}
respond("Warped to " + warpMsg + ".")
