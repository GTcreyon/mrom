//animation,anim|NUMBER|Sets the current character animation.
var animation = args[1]
if (animation == undefined) {
	animation = ""
}
respond("Animation: " + animation)
_root.Course.Char.attack = animation !== ""
_root.Course.Char.attackFrame = animation