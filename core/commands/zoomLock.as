//zoomlock,zl|wTOGGLE|Enables or disables zoom lock.
@SA 1 @SB _root.Maxzoomin !== Infinity @SC
if (wt) {
	_root.Maxzoomin = 200
	_root.Maxzoomout = 50
} else {
	_root.Maxzoomin = Infinity
	_root.Maxzoomout = -Infinity
}
togResp("Zoom Lock", @WG)