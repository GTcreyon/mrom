//cap|OPTION[invincible/vanish/metal/wing/none],wTOGGLE,*TIME|Sets the state of the caps the player currently has.
var cap = ""
var music = ""
switch(args[1].toLowerCase())
{
	case "invincible":
		music = ["WingCap - Intro", "WingCap - Repeat"]
		cap = "Invincible"
		break
	case "vanish":
	case "vanishcap":
	case "invisible":
		music = ["WingCap - Intro", "WingCap - Repeat"]
		cap = "Invisible"
		break
	case "metal":
	case "metalcap":
		music = ["MetalCap - Intro", "MetalCap - Repeat"]
		cap = "Metal"
		break
	case "wingcap":
	case "wing":
		music = ["WingCap - Intro", "WingCap - Repeat"]
		cap = "WingCap"
		break
}
if (cap === "") {
	_root.PowerTimer = 0
	_root.Invincible = false
	_root.Invisible = false
	_root.WingCap = false
	_root.PlayMusicAndIntro()
	respond("Cap removed.")
} else {
	@SA 2 @SB _root[cap] @SC
	_root[cap] = wt
	if (wt) {
		_root.StartNewMusicAndIntroNoVar(music[0], music[1])
		if (args[3] === undefined) {
			_root.PowerTimer = 10000
		} else {
			_root.PowerTimer = parseFloat(args[3])
		}
	} else {
		_root.PlayMusicAndIntro()
	}
	togResp(cap, @WG)
}
