//fludd,nozzle|OPTION[all/hover/rocket/turbo],wTOGGLE|Sets the availability state of FLUDD nozzles.
var wb = wBool(args[2])
var nozzle = args[1].toLowerCase()
var nozzID = ""
var nozzName = ""
switch(nozzle) {
	case "h":
	case "hover":
		nozzID = "H"
		nozzName = "Hover"
		break
	case "r":
	case "rocket":
		nozzID = "R"
		nozzName = "Rocket"
		break
	case "t":
	case "turbo":
		nozzID = "T"
		nozzName = "Turbo"
		break
	default:
		var valall = _root.SaveFluddH && _root.SaveFluddR && _root.SaveFluddT
		var wtall = wToggle(wb, valall)
		_root.SaveFluddH = wtall
		_root.SaveFluddR = wtall
		_root.SaveFluddT = wtall
		togResp("All FLUDD Nozzles", wb, valall, wtall)
		break
}
if (nozzID !== "") {
	var nozzRef = "SaveFludd" + nozzID
	var val = _root[nozzRef]
	var wt = wToggle(wb, val)
	_root[nozzRef] = wt
	togResp(nozzName, @WG)
}
