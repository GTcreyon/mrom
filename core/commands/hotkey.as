//hotkey,htk,hk|KEY,STRING|Add a hotkey for a given command.
var key = args[1]
var recombinedCommand = args.slice(2).join(" ")
var success = addHotkey(key, recombinedCommand)
if (success) {
	respond("Bound command to key \"" + key + "\".")
} else {
	respond("Failed to bind command to key \"" + key + "\". Key might not be valid.")
}
