//noclip,nc|wTOGGLE|Enables or disables NoClip.
@SA 1 @SB isNaN(_root.coursescale2) @SC
if (wt) {
	tempCourseScale = _root.coursescale2
	_root.coursescale2 = NaN
} else {
	if (tempCourseScale !== undefined)
	{
		_root.coursescale2 = tempCourseScale
	}
	else if (val)
	{
		_root.coursescale2 = 1
	}
}
togResp("NoClip", @WG)
