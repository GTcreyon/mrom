//showwarps,sw|wTOGGLE|Shows or hides warps (displayed as a black rectangle).
@SA 1 @SB _root.Course.BackGFX.warp1._visible @SC
var i = 1
while (i < 8) {
	_root.Course.BackGFX["warp" + i]._visible = wt
	i++
}
togResp("Show Warps", @WG)