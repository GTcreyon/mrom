shell.warpAlias = function(name) {
	switch (name.toLowerCase()) {
		//$WARPNAMES
	}
	// Capitalise the first character. Fixes some bugs with case-sensitivity.
	return name.charAt(0).toUpperCase() + name.slice(1)
}
shell.warpLocation = function(name) {
	switch (name) {
		case "2-5":
			return [140, -680.75]
		case "8-10":
			return [-290, 0]
		case "8-10-b":
			return [500, -10]
		case "6-1-2":
			return [2171, -163]
		case "M1-2":
			return [-1465, -320]
		case "M3-3":
			return [-1055.4, -119.75]
		case "4-5":
			return [-1300, 52.4]
		case "8-E5-1":
			return [-1693, -60]
		case "8-E5-2":
			return [1115, 127]
		case "8-16":
			return [0, -350]
		case "9-11":
			return [-560, -160]
		case "9-03-2":
			return [-270, -990]
		default:
			return [0, 0]
	}
}
