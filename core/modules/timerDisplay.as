shell.showTimer = false
shell.timerFrames = 0
shell.splitFrames = 0
shell.timerRunning = false
shell.timerPrepped = true
shell.timerDisplay = function() {
	if (!showTimer) {
		return undefined
	}
	var mainline = timeFormat(timerFrames)
	var splitline = timeFormat(splitFrames)
	return mainline + "\n" + splitline
}
shell.triggerTimerSplit = function() {
	splitFrames = timerFrames
	triggerPreppedTimer()
}
shell.triggerPreppedTimer = function() {
	if (timerPrepped) {
		timerRunning = true
		timerPrepped = false
	}
}
