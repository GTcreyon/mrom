shell.timeFormat = function(frames)
{
	var rawSeconds = frames / 32
	var hours = Math.floor(rawSeconds / 3600)
	rawSeconds -= 3600 * hours // Remove hours

	var numMinutes = rawSeconds / 60
	var strMinutes
	// Substring is a cheaper operation than modifying the string.
	// We can add 100 then take a substring to get a cheap zero-pad without string modification.
	// This also skips the flooring step, because we just truncate the string.
	numMinutes += 100
	var startIndex = 1
	if (hours > 0 && numMinutes < 10) {
		startIndex = 2
	}
	strMinutes = numMinutes.toString().substr(startIndex,2)

	rawSeconds %= 60 // Remove minutes
	var numSeconds = rawSeconds + 100
	var strSeconds = numSeconds.toString().substr(1,2)

	var numMilliseconds = rawSeconds % 1 * 1000 + 1000
	var strMilliseconds = numMilliseconds.toString().substr(1,3)
	if(hours > 0)
	{
		return hours + ":" + strMinutes + ":" + strSeconds + "." + strMilliseconds
	}
	return strMinutes + ":" + strSeconds + "." + strMilliseconds
}
