shell.showPecons = false

shell.drawCross = function(x, y, w, h) {
	moveTo(x,y - h)
	lineTo(x,y + h)
	moveTo(x - w,y)
	lineTo(x + w,y)
}

shell.drawTrack = function(x, y, repeats) {
	var index = 0
	while(index < repeats) {
		var pcx = (_root.Course.Char._x + (_root.Course.Char.wide - 1) * x + _root.Course.Char.xspeed / repeats * index) * (_root.coursescale / 100) + 225
		var pcy = (_root.Course.Char._y - _root.Course.Char.tall * (_root.Course.Char._yscale / 200 * y + 0.5) - 1.5 * (y - 1) + _root.Course.Char.yspeed / repeats * index) * (_root.coursescale / 100) + 150
		peconStyle(index, x, y, repeats)
		drawCross(pcx,pcy,4 - Math.abs(x) * 2, 4 - Math.abs(y) * 2)
		index += 1
	}
}

shell.drawPecons = function() {
	if(_root.Course.Char === undefined) {
		return
	}
	repeats = Math.ceil((Math.abs(_root.Course.Char.yspeed) + Math.abs(_root.Course.Char.xspeed)) / 5)
	repeats = Math.min(repeats,50)
	repeats = Math.max(repeats,1)
	drawTrack(-1, 0, repeats)
	drawTrack(1, 0, repeats)
	drawTrack(0, -1, repeats)
	drawTrack(0, 1, repeats)
}

shell.peconStyle = function(index, x, y, repeats) {
	switch(index) {
		case repeats - 1:
			lineStyle(2,65280,75,false,"none","none","round",1)
			break
		case 1:
			lineStyle(2,16776960 - 16711425 * (x+y),75,false,"none","none","round",1)
			break
		case 0:
			lineStyle(2,16711680,75,false,"none","none","round",1)
			break
	}
}
