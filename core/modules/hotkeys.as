shell.hotkeyCommands = []
var hotkeyListener = new Object()
hotkeyListener.onKeyDown = function () {
	if (!_root.blueshell.typing) {
		runHotkey(Key.getCode())
	}
}
shell.runHotkey = function(code) {
	var command = hotkeyCommands[code]
	if (command !== undefined) {
		executeCommand(command)
	}
}
shell.addHotkey = function(keyName, command) {
	var code = encodeKey(keyName)
	if (code === undefined) {
		return false
	} else {
		hotkeyCommands[code] = command
		return true
	}
}
shell.clearHotkey = function(keyName) {
	var code = encodeKey(keyName)
	hotkeyCommands[code] = undefined
}
shell.encodeKey = function(keyName) {
	var upperName = keyName.toUpperCase()
	var numcode = -1
	switch (upperName.length) {
		case 1:
			var ascode = upperName.charCodeAt(0)
			if (ascode > 47 && ascode < 91 && (ascode < 58 || ascode > 64)) {
				return ascode
			}
			break
		case 2:
			if (upperName[0] === "F") {
				numcode = upperName.charCodeAt(1) + 63
			}
			break
		case 3:
			if (upperName[0] === "N" && upperName[1] === "P") {
				numcode = upperName.charCodeAt(2) + 63
			}
			break
	}
	if (numcode > 47 && numcode < 58) {
		return numcode
	}
	switch (upperName) {
		//$KEYNAMES
	}
}
Key.addListener(hotkeyListener)
