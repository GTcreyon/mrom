shell.watches = []
shell.watchDisplay = function() {
	evalx = _root.Course.Char._x - _root.Course.FrontGFX._x
	evaly = _root.Course.Char._y - _root.Course.FrontGFX._y

	output = []
	if (watches.length === 0) {
		return undefined
	}
	var i = 0
	while (i < watches.length) {
		var name = watches[i]
		var realname = resolveWatchAlias(name)
		var value = eval(realname)
		output.push(name + ": " + value)
		i++
	}
	return output.join("\n")
}
shell.addWatch = function(name) {
	watches.push(name)
}
shell.removeWatch = function(name) {
	var i = 0
	while (i < watches.length) {
		if (watches[i] == name) {
			watches.splice(i, 1)
		}
		i++
	}
}
shell.resolveWatchAlias = function(name) {
	switch (name) {
		//$WATCHNAMES
	}
	return name
}
